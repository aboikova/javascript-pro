/*
Завдання 1
Створіть на сторінці div і дайте йому зовнішній відступ 150 пікселів. Використовуючи JS виведіть у консоль відступ.

Завдання 2
Створіть програму секундомір.
* Секундомір матиме 3 кнопки "Старт, Стоп, Скидання"
* При натисканні на кнопку стоп фон секундоміра має бути червоним, старт - зелений, скидання - сірий *
 Виведення лічильників у форматі ЧЧ:ММ:СС
* Реалізуйте Завдання використовуючи синтаксис ES6 та стрілочні функції
*/

/* Виконання завдання 1
const div = document.createElement("div");
document.body.prepend(div); 
console.log(getComputedStyle(div).margin);*/

// Завдання 2
const stopwatch = document.querySelector(".container-stopwatch");
let seconds = 0, minutes = 0, hours = 0, timer, isClick = false;

const get = id => document.getElementById(id); // функція отримання елемента по id

function updateTime (){
    seconds++;
    if (seconds > 59){
       minutes++;
       seconds = 0;
    }
    if (minutes > 59){
    hours++;
    minutes =0 ;
    }
    get('sec').innerText = `${seconds.toString().padStart(2,"0")}`
    get('mins').innerText = `${minutes.toString().padStart(2,"0")}`
    get('hours').innerText = `${hours.toString().padStart(2,"0")}`
}

get("start").onclick = () =>{
    clearClass();
    stopwatch.classList.add("green"); 
    if (!isClick){
  
    timer = setInterval(updateTime,1000);
    };
    
    isClick = true;
    
};

get("stop").onclick = () =>{
    clearClass();
    stopwatch.classList.add("red");
    clearInterval(timer);
    isClick = false;
 
};

get("reset").onclick = () =>{
    clearClass();
    stopwatch.classList.add("silver"); 
    isClick = false;
    clearInterval(timer);
    seconds = -1;
    minutes = 0;
    hours =0;
    updateTime();
  
}

function clearClass () {
    stopwatch.classList.remove("red");
    stopwatch.classList.remove("black");
    stopwatch.classList.remove("green");
    stopwatch.classList.remove("silver");
}





  


