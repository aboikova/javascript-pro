/*
Слайдер
Створіть слайдер кожні 3 сек змінюватиме зображення
Зображення для відображення
https://new-science.ru/wp-content/uploads/2020/03/4848-4.jpg
https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg
https://naukatv.ru/upload/files/shutterstock_418733752.jpg
https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg
https://nnst1.gismeteo.ru/images/2020/07/shutterstock_1450308851-640x360.jpg
*/

const arrImages = ["https://new-science.ru/wp-content/uploads/2020/03/4848-4.jpg",
"https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg",
"https://naukatv.ru/upload/files/shutterstock_418733752.jpg",
"https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg",
"https://nnst1.gismeteo.ru/images/2020/07/shutterstock_1450308851-640x360.jpg"];

const imgAltName = ["img1","img2","img3","img4","img5"]
let i = 0;

const div = document.createElement("div")
img = document.createElement("img");
document.body.prepend(div);
div.append(img);
img.src = `${arrImages[i]}`;
img.alt = `${imgAltName[i]}`;

setInterval(() => {
    i++;
    if (i>=arrImages.length && i>=imgAltName.length){
        i=0;
    };
    img.src = arrImages[i];
    img.alt = `${imgAltName[i]}`;
}, 3000);


