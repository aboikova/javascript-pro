/* Задача 1 
Напиши функцию map(fn, array), которая принимает на вход функцию
и массив, и обрабатывает каждый элемент массива этой функцией,
возвращая новый массив.*/

const arr=[3,6,9,7,44];

function addOneArr(i){
   return i+1;
}

function map(addOneArr,arr){
    let newArr = [];
    for(let i = 0; i < arr.length; i++){
        newArr.push(addOneArr(arr[i]));
    }
    return newArr;
}
console.log(map(addOneArr,arr));

/* Задача 2
Перепишите функцию, используя оператор '?' или '||'
Следующая функция возвращает true, если параметр age больше 18.
В ином случае она задаёт вопрос confirm и возвращает его результат.
1 function checkAge(age) {
2 if (age > 18) {
3 return true;
4 } else {
5 return confirm('Родители разрешили?');
6 } }*/

// з оператором '||'
function checkAge(age) {
    return age > 18 || confirm('Родители разрешили?')
    };

 console.log(checkAge(15));
 console.log(checkAge(20));

// з оператором '?'

 function checkAge(age) {
    return age > 18 ? true : confirm('Родители разрешили?')
    };

 console.log(checkAge(15));
 console.log(checkAge(20));
