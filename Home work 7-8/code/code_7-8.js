/*
Створити клас Car , Engine та Driver.
Клас Driver містить поля - ПІБ, стаж водіння.

Клас Engine містить поля – потужність, виробник.

Клас Car містить поля – марка автомобіля, клас автомобіля, вага, водій типу Driver,
мотор типу Engine. Методи start(), stop(), turnRight(), turnLeft(), які виводять
на друк: "Поїхали", "Зупиняємося", "Поворот праворуч" або "Поворот ліворуч".
А також метод toString(), який виводить повну інформацію про автомобіль, її водія і двигуна.

Створити похідний від Car клас - Lorry (вантажівка), що характеризується також вантажопідйомністю кузова.
Створити похідний від Car клас - SportCar, який також характеризується граничною швидкістю.
*/


// Створити клас Engine
class Engine {
    constructor (powerEngine, company){
        this.powerEngine = powerEngine;
        this.company = company;
    }
    toString(){
        document.write((`${this.powerEngine} </br>`));
    }
}
 
const power1 = new Engine ("V8", "Salmson");

//power1.toString();

//// Створити клас Driver
class Driver extends Engine {
    constructor(powerEngine, company, fullName, drivingExperience) {
        super(powerEngine, company);
    this.fullName = fullName;
    this.drivingExperience = drivingExperience;
    }
    toString(){
        document.write((`${this.fullName}, ${this.drivingExperience}, ${this.powerEngine} </br>`));
    }
}

const driver1 = new Driver ("V8", "Salmson", "Петро Петрович Толочко", "5 років");
 
//driver1.toString();


//Створити клас Car
class Car extends Driver {
    constructor (powerEngine, company, fullName, drivingExperience, carBrand, carClass, carWeight){
        super(powerEngine, company, fullName, drivingExperience);
        this.carBrand = carBrand;
        this.carClass = carClass;
        this.carWeight = carWeight;
   
    }
    start(){
       document.write("Поїхали" + "</br>");
    
    }
   
    stopt(){
        document.write("Зупиняємося" + "</br>");
    }

     turnRight(){
        document.write("Поворот праворуч" + "</br>");
    }

    turnLeft(){
        document.write("Поворот ліворуч" + "</br>");
    }

    toString(){
        document.write((`<p>Водій: ${this.fullName} </br>
                         Марка автомобіля: ${this.carBrand} </br>
                         Клас автомобіля: ${this.carClass} </br>
                         Вага автомобіля,кг: ${this.carWeight} </br>
                         Мотор типу: ${this.powerEngine}  </br></p>`));

    }

}

const car1 = new Car ("V8", "Salmson", "Петро Петрович Толочко", "5 років", "Mazda Moto", "Turbo", 200 );

car1.start();
car1.stopt();
car1.turnRight();
car1.turnLeft();
car1.toString();

//Створити похідний від Car клас - Lorry (вантажівка), що характеризується також вантажопідйомністю кузова

class Lorry extends Car{
    constructor(powerEngine, company, fullName, drivingExperience, carBrand, carClass, carWeight, carryingCapacity){
        super(powerEngine, company, fullName, drivingExperience, carBrand, carClass, carWeight);
        this.name = "Lorry";
        this.carryingCapacity = carryingCapacity;
    }
    toString(){
        document.write((`<p> ${this.name} </br>
                         Марка автомобіля: ${this.carBrand} </br>
                         Клас автомобіля: ${this.carClass} </br>
                         Вага автомобіля,кг: ${this.carWeight} </br>
                         Вантажопідйомністю кузова: ${this.carryingCapacity}  </br></p>`));

    }
}

const lorry1 = new Lorry ("V8", "Salmson", "Петро Петрович Толочко", "5 років", "Toyota", "Turbo", 200, 1500);

lorry1.toString();



//Створити похідний від Car клас - SportCar, який також характеризується граничною швидкістю.


class SportCar extends Car {
    constructor(powerEngine, company, fullName, drivingExperience, carBrand, carClass, carWeight, maxSpeed ){
        super(powerEngine, company, fullName, drivingExperience, carBrand, carClass, carWeight);
        this.name = "SportCar";
        this.maxSpeed = maxSpeed ;
    }
    toString(){
        document.write((`<p> ${this.name} </br>
                         Марка автомобіля: ${this.carBrand} </br>
                         Клас автомобіля: ${this.carClass} </br>
                         Вага автомобіля,кг: ${this.carWeight} </br>
                         Гранична швидкість: ${this.maxSpeed}  </br></p>`));

    }
}

const sportCar1 = new SportCar ("V8888", "Salmson 365", "Данило Петрович", "10 років", "Porsche 911", "Luxury", 200, 350);


sportCar1.toString();


