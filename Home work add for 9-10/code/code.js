/*## Задание

Написать реализацию кнопки "Показать пароль".

#### Технические требования:
- В файле `index.html` лежит разметка для двух полей ввода пароля. 
- По нажатию на иконку рядом с конкретным полем - должны отображаться символы, которые ввел пользователь, иконка меняет свой внешний вид.
- Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)
- Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)
- По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях
- Если значения совпадают - вывести модальное окно (можно alert) с текстом - `You are welcome`;
- Если значение не совпадают - вывести под вторым полем текст красного цвета  `Нужно ввести одинаковые значения`
- После нажатия на кнопку страница не должна перезагружаться
- Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.
*/


const input1 = document.getElementById("input1"),
input2 = document.getElementById("input2"),
clickEye = document.getElementById("fa_eye"),
clickEyeSlash = document.getElementById("fa_eye_slash"),
[...newType] = document.querySelector("[type]"),
arrType = ["text","password"],
//openEye = querySelector(".fa-eye:before"),
[btn] = document.getElementsByClassName("btn");
let i =0;
let n = 0;



btn.onclick = () =>{
if (input1.value === input2.value && input1.value !== '' ){
    alert("You are welcome");
}else{
    const divNew = document.createElement("div");
    divNew.textContent = "Необхідно ввести однакові значення";
    input2.after(divNew);
}
};


clickEye.onclick = () =>{
    
        if(clickEye.getAttribute("class") === "fas fa-eye icon-password"){
            clickEye.className = "fas fa-eye-slash  icon-password";
        }else 
            clickEye.className = "fas fa-eye icon-password";
       
    
    newType[0].setAttribute("type", `${arrType[i]}`);
    i++;
    if (i >= arrType.length){
        i=0;
        
    };
};
   

clickEyeSlash.onclick = () =>{

   if(clickEyeSlash.getAttribute("class") === "fas fa-eye icon-password"){
        clickEyeSlash.className = "fas fa-eye-slash icon-password";
    }else 
    clickEyeSlash.className = "fas fa-eye icon-password";

    newType[1].setAttribute("type", `${arrType[n]}`);
    n++;
    if (n >= arrType.length){
        n=0;
        
    };
}
