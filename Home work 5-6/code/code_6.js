/* Задание

Разработайте функцию-конструктор, которая будет создавать объект Human (человек).
Создайте массив объектов и реализуйте функцию, которая будет сортировать элементы 
массива по значению свойства Аgе по возрастанию или по убыванию.

Добавьте на свое усмотрение свойства и методы в этот объект.
Подумайте, какие методы и свойства следует сделать уровня экземпляра,
а какие уровня функции-конструктора. */


// Функція-конструктор для створення об'єктів Human

function Human (nameUser, sex, job, age) {

  this.nameUser = nameUser;
  this.sex = sex;
  this.job = job;
  this.age = age;
 
  Human.maxHumanCount++;
   
  };

  // Створення екземплярів
  Human.maxHumanCount=0; // Статична властивість
  const user1 = new Human ("Алекс", "чол", "Програміст", 28);
  const user2 = new Human ("Зіна", "жін", "Художник", 18);
  const user3 = new Human ("Петро", "чол", "Військовий" , 35);
  Human.city = "Kyiv"; // Статична властивість city, яка не наслідується екземпляром
  
 
  console.log(`Кількість людей на обліку: ` + Human.maxHumanCount);
  console.log(Human.city);

Human.prototype.aboutUser = function(){
  return (`User: ${this.nameUser}, ${this.sex}, ${ this.job}, ${this.age} </br>`);
};

document.write(user1.aboutUser());
document.write(user2.aboutUser());
document.write(user3.aboutUser());

// Створення масиву
const dataPeople =[
  new Human ("Алекс", "чол", "Програміст", 28),
  new Human ("Зіна", "жін", "Художник", 18),
  new Human ("Петро", "чол", "Військовий", 35)
];

dataPeople.sort(function(a,b){
  return a.age - b.age;
 // return b.age - a.age; - Якщо потрібно по спаданню
});

console.log(`Відсортовані елементи масиву за властивостю Аgе по зростанню:`);
console.log(dataPeople);


 

