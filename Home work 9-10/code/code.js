/*
Завдання

Намалювати на сторінці коло, використовуючи параметри, які введе користувач.
При завантаженні сторінки - показати на ній кнопку з текстом "Намалювати коло". 
Дана кнопка має бути єдиним контентом у тілі HTML документу, весь інший контент повинен бути створений і доданий через JavaScript.

При натисканні на кнопку "Намалювати коло" з'являється одне поле вводу - діаметр кола. При натисканні на кнопку
 "Намалювати" на сторінці створюється 100 шт. кіл (10х10) випадкового кольору.
При кліку на конкретне коло, це коло повинне зникнути, при цьому пусте місце заповнюється, тобто усі інші кола зсуваються вліво.
*/


function createElement (elementName, className){
 const element = document.createElement(elementName);

        if (className !=="" || className !== null){
        element.className = className;
    }

    return element;
}

const button = document.getElementById("btn"),
input = createElement("input", "input_info"),
container = createElement("div", "container");
//div = createElement("div","circle");
input.placeholder = "Введіть радіус кола";
document.body.prepend(container);




button.onclick = function(){
   
   
        if (isNaN(input.value) || input.value === ''){
        input.value = "";
        return alert("Введіть радіус кола");
        }
        
        let  widthA = `${input.value}px`;
        let heightB = `${input.value}px`;
        
        for (let i = 0; i < 100; i++){
            const div = createElement("div","circle");
            container.append(div);
            let [...circle] = document.getElementsByClassName("circle");
            circle.forEach(function(item){
            item.style.backgroundColor = `hsl(${Math.floor(Math.random()*360)}, 50%, 80%)`; 
            item.style.width = widthA;
            item.style.height = heightB;
            item.onclick = function(){
                // alert(item.textContent);
              //console.log("+");
              item.remove();
             };
         
});

    } 
         
};


window.onload = () => {
    
    button.after(input);
    input.after(container);
     
}







